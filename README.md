# plantuml-gradle-plugin: Generate diagrams using plantuml

[PlantUML](http://plantuml.com/) is a great tool allowing to generate UML diagrams from an easy to write text files.
This plugin allows to indicate in the build file which images should be generated.

## Dependencies

PlantUML requires [Graphviz](http://graphviz.org/) to be installed. See
[compatibility notes](http://plantuml.com/graphviz-dot) for more details.


## Add the plugin to the build

```groovy
buildscript {
  repositories {
    maven {
      url "https://plugins.gradle.org/m2/"
    }
    mavenCentral()
  }
  
  dependencies {
    classpath "org.fmiw:plantuml-gradle-plugin:0.1"
  }
}
```
the apply the plugin to your project with:

```groovy
apply plugin: "org.fmiw.plantuml"
```

Alternatively, plugin can be imported using the
[new plugins DSL](https://docs.gradle.org/current/userguide/plugins.html#sec:plugins_block):

```groovy
plugins {
  id "org.fmiw.plantuml" version "0.1"
}
```

## How to use

The plugin adds a task ``generateDiagrams`` generating all diagrams declared in the build.gradle file, using the
following syntax:


```groovy
plantuml {
  options {
    outputDir = project.file('out')
  }
  
  diagrams {
    file1 {
      sourceFile = project.file('my-file1.plantuml')
    }
    file2 {
      sourceFile = project.file('file2.plantuml')
    }
  }
}
```
 
With the above, when calling ``generateDiagrams``, two files will be generated in the ``out`` directory:

- ``file1.svg`` from the file ``my-file1.plantuml``,
- ``file2.svg`` from the file ``file2.plantuml``.

Note that a task is generated for each diagram in ``diagrams`` node. Therefore, it is possible to regenerate the file1
output using the ``generateDiagramFile1`` task.

## Options

Some configuration options can be passed through the ``options {}`` node:

- ``outputDir`` (default ``$buildDir/plantuml``): a directory in which to write the images,
- ``format`` (default ``"svg"``): the output format.

# License

This project is licensed under the MIT license.

