package org.fmiw.plantuml

import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder


class PlantUMLPluginTest {
  @Rule
  public final TemporaryFolder testProjectDir = new TemporaryFolder()

  private File buildFile

  private File outputDir

  private File sourceFile

  @Before
  void setup() throws IOException {
    buildFile = testProjectDir.newFile("build.gradle")
    outputDir = testProjectDir.newFolder("output")
    sourceFile = testProjectDir.newFile("file1.plantuml")
    sourceFile << """
@startuml
title Client send new order and trader accepts it.
Client -> OMS : insert ORDER
    """
  }

  @Test
  void testGenerateTask() throws IOException {
    buildFile << """
    plugins {
      id "org.fmiw.plantuml"
    }
    
    plantuml {
      options {
        outputDir = project.file("${outputDir}")
      }

      diagrams {
        file1 {
          sourceFile = project.file("${sourceFile}")
        }
      }
    }
    """

    BuildResult result =
        GradleRunner.create()
            .withProjectDir(testProjectDir.getRoot())
            .withPluginClasspath()
            .withArguments("--stacktrace", "generateDiagrams")
            .build()

    println("Contents of ${outputDir.absolutePath}")
    outputDir.eachFile {
      println("Found file ${it}")
    }
    Assert.assertTrue(new File(outputDir, "file1.svg").exists())
  }

  @Test
  void testGeneratePNG() throws IOException {
    buildFile << """
    plugins {
      id "org.fmiw.plantuml"
    }
    
    plantuml {
      options {
        outputDir = project.file("${outputDir}")
        format = "png"
      }

      diagrams {
        file1 {
          sourceFile = project.file("${sourceFile}")
        }
      }
    }
    """

    BuildResult result =
        GradleRunner.create()
            .withProjectDir(testProjectDir.getRoot())
            .withPluginClasspath()
            .withArguments("--stacktrace", "generateDiagrams")
            .build()

    println("Contents of ${outputDir.absolutePath}")
    outputDir.eachFile {
      println("Found file ${it}")
    }
    Assert.assertTrue(new File(outputDir, "file1.png").exists())
  }
}


