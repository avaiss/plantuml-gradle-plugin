package org.fmiw.plantuml

import net.sourceforge.plantuml.FileFormat
import net.sourceforge.plantuml.FileFormatOption
import net.sourceforge.plantuml.SourceStringReader
import org.gradle.api.DefaultTask
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.tasks.InputFile
import org.gradle.api.tasks.Nested
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction

/**
 * This plugin adds a 'generateDiagrams' task that will generate an image for each source file given in configuration.
 */
class PlantUMLPlugin implements Plugin<Project> {

  @Override
  void apply(Project project) {
    def plantuml = project.extensions.create('plantuml', PlantUML)

    def opt = plantuml.extensions.create('options', Options)
    opt.format = "svg"
    opt.outputDir = project.file("${project.buildDir}/plantuml")

    def diagrams = project.container(Diagram)
    plantuml.extensions.diagrams = diagrams

    // The main task.
    Task generate = project.task("generateDiagrams")

    // For each diagram, create a task for generating this diagram. Then add it as dependency of the main task.
    plantuml.diagrams.all {
      Diagram d = delegate

      Task task = project.task("generateDiagram${d.name.capitalize()}", type: GenerateDiagramTask) {
        description "Generate image for ${d.name}"
        group 'plantuml'
        diagram d
        options = opt
      }

      generate.dependsOn task
    }
  }
}

/**
 * Defines options for the plugin.
 */
class Options {
  /** Where to generate the output image. */
  File outputDir

  /** Output format. */
  String format

  /** Gets PlantUML file format from given format. */
  FileFormat getFileFormat() {
    FileFormat.valueOf(FileFormat, format.toUpperCase())
  }
}

/**
 * A diagram to generate.
 */
class Diagram {
  /** The diagram unique name. */
  final String name

  /** Source file. */
  @InputFile
  File sourceFile

  Diagram(String name) {
    this.name = name
  }
}

class PlantUML {
}

/**
 * The task generating an image from a plantuml source file.
 */
class GenerateDiagramTask extends DefaultTask {
  @Nested
  Diagram diagram

  @Nested
  Options options

  @OutputFile
  File getOutputFile() {
    new File(options.outputDir, diagram.name + options.fileFormat.fileSuffix)
  }

  @TaskAction
  void generate() {
    def reader = new SourceStringReader(diagram.sourceFile.text)

    def parentFile = outputFile.parentFile
    if (!parentFile.exists() && !parentFile.mkdirs())
      throw new Exception("Unable to create ${outputFile.parentFile} directory")

    outputFile.withOutputStream {
      reader.outputImage(it, new FileFormatOption(options.fileFormat))
    }
  }
}